# No, You Decide!

A site to help you decide what to eat.

https://matthewwilliamson93.gitlab.io/no-you-decide/

## Developing

### Development Run

To run the code for local development:

```
make run
```

Runs the app in the development mode.\
Opens [http://localhost:3000](http://localhost:3000) to view.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### Production Run

If you are looking to run the the code for release:

```
make prod-run
```

This will build the app in a production mode and serve it from a local NGINX docker image. \
Open [http://localhost](http://localhost) to view the site.

The page will **NOT** reload if you make edits.

### All else

Generally the repo uses a `Makefile` if you want to see
other commands simply run `make`.

```
Usage:
    help:            Prints this screen
    install-deps:    Installs dependencies
    upgrade:         Upgrades the dependencies
    anaylze-bundle:  Analyzes the JS bundle
    check-fmt:       Checks the formatting of the code
    fmt:             Formats the code
    type-check:      Type checks the code
    lint:            Lints the code
    fix-lint:        Automatically applies certain lint fixes
    build:           Build the frontend for deployment
    run:             Run the frontend in a dev mode
    prod-build:      Build the docker image for the prod mode
    prod-full-build: Build the docker image for the prod mode
    prod-run:        Run the prod mode
    clean:           Clean out temporaries
```

## CI / CD

The repo uses a local gitlab CI config to build the code and deploy it to gitlab pages.

## Project Layout

For those trying to get more familar with the setup.

```
.
├── Makefile
├── README.md
├── craco.config.js
├── deployment
│   ├── Dockerfile
│   └── app.conf
├── package.json
├── public
│   ├── favicon.svg
│   ├── index.html
│   ├── manifest.json
│   └── robots.txt
├── src
│   ├── App.tsx
│   ├── components
│   │   ├── AddRestaurantModal.tsx
│   │   └── RestaurantSelector.tsx
│   ├── hooks
│   │   └── useLocalStorage.ts
│   ├── index.tsx
│   ├── registerServiceWorker.ts
│   ├── types
│   │   └── restaurant.ts
│   └── utils
│       └── browser.ts
└── tsconfig.json
```
