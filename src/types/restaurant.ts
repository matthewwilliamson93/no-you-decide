export type Restaurant = {
    name: string;
    url: string;
};
