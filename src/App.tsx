import Grid from "@mui/material/Grid";

import { createStyles, makeStyles } from "@mui/styles";

import React from "react";

import { useLocalStorage } from "./hooks/useLocalStorage";
import type { Restaurant } from "./types/restaurant";

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            background: "white",
            flexGrow: 1,
            height: "100vh",
            position: "relative",
        },
        header: {
            "margin-bottom": "50px !important",
            font: '375 3.2em/1.2 "Lato", sans-serif',
            color: "white",
            margin: 0,
            position: "relative",
            "text-align": "center",
        },
    })
);

/*
 * A React component that is meant to house the main layout for the site.
 *
 * https://mui.com/components/grid/
 * https://mui.com/styles/api/
 */
export default function App(): JSX.Element {
    const header = "No, You Decide!";
    
    const classes = useStyles();

    const [isHome, setIsHome] = useLocalStorage<boolean>(true, "nyd-is-home");
    const [restaurants, setRestaurants] = useLocalStorage<Restaurant[]>([], "nyd-restaurants");

    return (
        <div className={classes.root}>
           <Grid alignItems="center" container justifyContent="center" gap={2}>
                <Grid item xs={12}>
                    <h1 className={classes.header}>{header}</h1>
                </Grid>
            </Grid>
            <Grid
                alignItems="center"
                container
                justifyContent="flex-start"
                gap={2}
            >
                <Grid item xs={3}>
                    <TravelInput
                        route={isHome ? previousDestinations : route}
                        setRoute={isHome ? setPreviousDestinations : setRoute}
                    />
                </Grid>
                <Grid item xs={2}>
                    {isHome ? null : (
                        <TravelPlannerButton
                            route={route}
                            setRoute={setRoute}
                        />
                    )}
                </Grid>
            </Grid>
            <Grid
                alignItems="stretch"
                container
                justifyContent="flex-start"
                gap={2}
            >
                <Grid item xs={3}>
                    <TravelList
                        isHome={isHome}
                        setIsHome={setIsHome}
                        route={isHome ? previousDestinations : route}
                        setRoute={isHome ? setPreviousDestinations : setRoute}
                    />
                </Grid>
           </Grid>
        </div>
    );
}
