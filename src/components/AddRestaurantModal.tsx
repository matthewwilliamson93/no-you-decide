import { useState } from "react";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import From from "@mui/material/Form";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";

import type { Restaurant } from "../types/restaurant";

const style = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
};

export default function AddRestaurantModal() {
    const [open, setOpen] = useState(false);

    return (
        <div>
            <Button
                onClick={() => handleOpen(true)}
            >
                Open modal
            </Button>
            <Modal
                open={open}
                onClose={() => setOpen(false)}
		aria-labelledby="Add a Restaurant"
		aria-describedby="A modal to have a user add a restaurant to the list."
            >
                <Box
                    component="form"
                    sx={style}
                >
                    <Typography id="add-restaurant-modal-title" variant="h6" component="h2">
                        Add a Restaurant
                    </Typography>
		    <TextField
                        placeholder="Name"
                        required
                    />
		    <TextField
                        placeholder="URL"
                        required
                        inputProps={{ pattern: /^[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/i }}
                    />
                </Box>
            </Modal>
        </div>
    );
}
